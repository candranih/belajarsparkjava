/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.candra.belajarspark;

import com.candra.belajarspark.controller.LoginController;
import com.candra.belajarspark.controller.PersonController;
import com.candra.belajarspark.dao.impl.PersonDaoImpl;
import com.candra.belajarspark.datasource.DataSource;
import com.candra.belajarspark.model.Person;
import com.google.gson.Gson;
import java.sql.Connection;
import static spark.Spark.*;

/**
 *
 * @author candra.hermanto@gmail.com
 */
public class App {
    
    public static void main(String args[]) {

    
        
      /**
       * paramater koneksi database
      */
       String databaseUsername = "belajarspark";
       String databasePassword = "sparkjava";
       String databaseHost = "localhost";
       String databasePort ="1521";
       String databaseSID = "siakdb";         
        
       DataSource  dataSource = new DataSource();
       dataSource.setUsername(databaseUsername);
       dataSource.setPassword(databasePassword);
       dataSource.setDatabaseHost(databaseHost);
       dataSource.setDatabasePort(databasePort);
       dataSource.setDatabaseSID(databaseSID);
       //Connection connection = dataSource.getConnection();
        
        
       staticFileLocation("/public"); 
       get("/", (req, res) -> {
           res.redirect("/login");
           return "";
       });

       new LoginController();
       new PersonController(new PersonDaoImpl(dataSource));
       
       before("/app/*",(req,res)->{
           System.out.println("session"+req.session().attribute("username"));
           if(req.session().attribute("username")==null) {
               halt(404,"Access Denied");
           }
       });
       
    }
            


}
