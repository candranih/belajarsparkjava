/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.candra.belajarspark.datasource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author candra.hermanto@gmail.com
 */
public class DataSource {
    private String username;
    private String password;
    private String databaseHost;
    private String databasePort;
    private String databaseSID;
    
    private static final String  ORACLE_DRIVER="oracle.jdbc.driver.OracleDriver";

    /**
     * register oracle driver
     */
    public DataSource() {
        try {
            try {
                Class.forName(ORACLE_DRIVER).newInstance();
            } catch (InstantiationException ex) {
                Logger.getLogger(DataSource.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(DataSource.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataSource.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public Connection getConnection() {
        String  databaseUrl = "jdbc:Oracle:thin:@//"+databaseHost+":"+databasePort+"/"+databaseSID;
        try {
            return DriverManager.getConnection(databaseUrl, username, password);
        } catch (SQLException ex) {
            Logger.getLogger(DataSource.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatabaseHost() {
        return databaseHost;
    }

    public void setDatabaseHost(String databaseHost) {
        this.databaseHost = databaseHost;
    }

    public String getDatabasePort() {
        return databasePort;
    }

    public void setDatabasePort(String databasePort) {
        this.databasePort = databasePort;
    }

    public String getDatabaseSID() {
        return databaseSID;
    }

    public void setDatabaseSID(String databaseSID) {
        this.databaseSID = databaseSID;
    }
    
    
    
}
