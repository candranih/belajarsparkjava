/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.candra.belajarspark.controller;
import static spark.Spark.*;

/**
 *
 * @author candra.hermanto@gmail.com
 */
public class LoginController {
    
    public LoginController() {
        
        get("/login", (req, res) -> {
           res.redirect("/login.html");
           return "";
        });
        
        post("/login", (req, res)-> {
            String username = req.queryParams("username");
            String password = req.queryParams("password");

            if(username.equals("candra") && password.equals("hermanto")) {
                req.session().attribute("username", username);
                res.redirect("/app/person/list");
                return "";
            } else {
                halt(404, "usename salah");
                return "";
            }
        });
    }

}
