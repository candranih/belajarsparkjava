/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.candra.belajarspark.controller;

import com.candra.belajarspark.dao.PersonDao;
import com.google.gson.Gson;
import static spark.Spark.*;

/**
 *
 * @author candra.hermanto@gmail.com
 */
public class PersonController {
    
    public PersonController(PersonDao personDao) {
        get("/app/person/list", (req, res) -> {
           
            return new Gson().toJson(personDao.list());
        });
        
        get("/app/person/add", (req,res) -> {
            return "";
        });
    }

}
