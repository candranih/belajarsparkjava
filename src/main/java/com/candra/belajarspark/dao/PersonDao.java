/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.candra.belajarspark.dao;

import com.candra.belajarspark.model.Person;
import java.util.List;

/**
 *
 * @author candra.hermanto@gmail.com
 */
public interface PersonDao {

    public List<Person> list();
    public Person getPerson(long id);
    public void save(Person person);
    public void delete(long id);
}
