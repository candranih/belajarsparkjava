/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.candra.belajarspark.dao.impl;

import com.candra.belajarspark.dao.PersonDao;
import com.candra.belajarspark.datasource.DataSource;
import com.candra.belajarspark.model.Person;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author candra.hermanto@gmail.com
 */

public class PersonDaoImpl implements PersonDao {
    
    private DataSource dataSource;
    
    public PersonDaoImpl(DataSource dataSource) {  
        this.dataSource = dataSource;
    }
    
    @Override
    public List<Person> list() {
        List<Person> listPerson = new ArrayList<>();
        Connection con = dataSource.getConnection();
        try {
            String query = "select * from person";
            PreparedStatement ps = con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                Person p = new Person();
                p.setId(rs.getLong("ID"));
                p.setName(rs.getString("NAME"));
                p.setEmailAddress(rs.getString("EMAIL_ADDRESS"));
                listPerson.add(p);
            }
            return listPerson;
        } catch (SQLException ex) {
            Logger.getLogger(PersonDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            try {
                con.close();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
       
    }

    @Override
    public Person getPerson(long id) {
        Connection con = dataSource.getConnection();
        try {
            String query = "select * from person where id=?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                Person p = new Person();
                p.setId(rs.getLong("ID"));
                p.setName(rs.getString("NAME"));
                p.setEmailAddress(rs.getString("EMAIL_ADDRESS"));
                return p;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            try {
                con.close();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        
       
    }

    @Override
    public void save(Person person) {
        Connection con = dataSource.getConnection();
        try {
            String query = "insert into person (id, name, email_address) values (?,?,?)";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setLong(1, person.getId());
            ps.setString(2, person.getName());
            ps.setString(3, person.getEmailAddress());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PersonDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
       
    }

    @Override
    public void delete(long id) {
        Connection con = dataSource.getConnection();
        try {
            String query = "delete from person where id=?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PersonDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
       
    }

}
